package br.com.senac.cachoeira.view;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.opengl.EGLExt;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.dao.CachoeiraDAO;
import br.com.senac.cachoeira.model.Cachoeira;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_NOVO = 1;


    public static final String CACHOEIRA = "cachoeira";
    public static final String IMAGEM = "imagem";
    private Cachoeira cachoeiraselecionada;


    private ListView listView;
    private List<Cachoeira> lista = new ArrayList<>();
    private ArrayAdapter<Cachoeira> adapter;

    private CachoeiraDAO dao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //criar listview
        listView = findViewById(R.id.ListaCachoeiras);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posicao, long l) {
                cachoeiraselecionada = (Cachoeira) adapterView.getItemAtPosition(posicao);
                return false;
            }


        });
        registerForContextMenu(listView);
        /* pegar elementos da view */


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                Cachoeira cachoeira = (Cachoeira) adapter.getItemAtPosition(posicao);

                Intent intent = new Intent(MainActivity.this, DetalheActivity.class);
                intent.putExtra(CACHOEIRA, cachoeira);


                startActivity(intent);


            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();


         /* pegar elementos da view */
        listView = findViewById(R.id.ListaCachoeiras);

        dao = new CachoeiraDAO(this);
        lista = dao.getLista();
        dao.close();

        int layout = android.R.layout.simple_list_item_1;

        adapter = new ArrayAdapter<Cachoeira>(this, layout, lista);

        listView.setAdapter(adapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    public void novo(MenuItem item) {

        Intent intent = new Intent(this, NovoActivity.class);
        startActivityForResult(intent, REQUEST_NOVO);


    }


    public void sobre(MenuItem item) {
        Intent intent = new Intent(this, SobreActivity.class);
        startActivity(intent);


    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem menueditar = menu.add("Editar");
        menueditar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(MainActivity.this, NovoActivity.class);
                intent.putExtra(CACHOEIRA, cachoeiraselecionada);
                startActivity(intent);
                return false;
            }
        });
    }
}
