package br.com.senac.cachoeira.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import br.com.senac.cachoeira.R;
import br.com.senac.cachoeira.dao.CachoeiraDAO;
import br.com.senac.cachoeira.model.Cachoeira;

public class NovoActivity extends AppCompatActivity {

    public static final int REQUEST_IMAGE_CAPTURE = 1;

    public static final int CAPTURA_IMAGEM = 1;

    private TextView textview;

    private String nomearquivo;

    private static Bitmap imagem;

    public static Bitmap getImagem() {
        return imagem;
    }


    private ImageView imageView;
    private EditText editTextNome;
    private EditText editTextInformacoes;
    private RatingBar ratingBarClassificacao;
    private Button buttonSalvar;

    private CachoeiraDAO dao;
    private Cachoeira cachoeira;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);
        imageView = findViewById(R.id.foto);
        editTextNome = findViewById(R.id.nome);
        editTextInformacoes = findViewById(R.id.informacoes);
        ratingBarClassificacao = findViewById(R.id.rtClassificacao);
        buttonSalvar = findViewById(R.id.btnSalvar);


        Intent intent = getIntent();

        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);

        if (cachoeira == null) {
            cachoeira = new Cachoeira();
        }


        editTextNome.setText(cachoeira.getNome());
        editTextInformacoes.setText(cachoeira.getInformocoes());
        ratingBarClassificacao.setRating(cachoeira.getClassificacao());

        if (cachoeira.getImagem() != null) {
            Bitmap fotoorigem = BitmapFactory.decodeFile(cachoeira.getImagem());
            Bitmap fotopadrao = Bitmap.createScaledBitmap(fotoorigem, 200, 200, true);

            imageView.setImageBitmap(fotopadrao);
        }


    }


    public void capturarImagem(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String caminhoexternodoarquivo = Environment.getExternalStorageDirectory().toString();


        nomearquivo = caminhoexternodoarquivo + "/" + System.currentTimeMillis() + ".png;";

        File arquivo = new File(nomearquivo);

        Uri localarmazenamento = Uri.fromFile(arquivo);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, localarmazenamento);

        startActivityForResult(intent, CAPTURA_IMAGEM);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (CAPTURA_IMAGEM == requestCode) {
            if (resultCode == RESULT_OK) {

                Bitmap fotooriginal = BitmapFactory.decodeFile(nomearquivo);
                Bitmap fotoreduzida = Bitmap.createScaledBitmap(fotooriginal, 200, 200, true);

                imageView.setImageBitmap(fotoreduzida);
            }

        }


    }


    public void salvar(View view) {

        String nome = editTextNome.getText().toString();
        String informacao = editTextInformacoes.getText().toString();
        float classificacao = ratingBarClassificacao.getRating();


        cachoeira.setNome(nome);
        cachoeira.setInformocoes(informacao);
        cachoeira.setClassificacao(classificacao);
        cachoeira.setImagem(nomearquivo);

       /*Adicionando no banco*/
        try {
            dao = new CachoeiraDAO(NovoActivity.this);
            dao.salvar(cachoeira);
            dao.close();
            Toast.makeText(NovoActivity.this, "Sucesso!", Toast.LENGTH_LONG).show();

            finish();
        } catch (Exception ex) {
            Toast.makeText(NovoActivity.this, "Errou!", Toast.LENGTH_LONG).show();

        }


        // voltar main activity
        Intent intent = new Intent();
        intent.putExtra(MainActivity.CACHOEIRA, cachoeira);
        setResult(RESULT_OK, intent);

        finish();


    }


}
